""" Deployement script to create a config file containing RP_API_ADDRESS set up
with the HOST_IP_OR_DOMAIN environment variable. """
from os import environ


HOST_IP_OR_DOMAIN = environ.get("HOST_IP_OR_DOMAIN")
RP_API_ADDRESS = f'https://rp-backend.{HOST_IP_OR_DOMAIN}'

jsText = (
    f"var config = {{ RP_API_ADDRESS: '{RP_API_ADDRESS}', }}"
)
with open("config.js", "w") as js_file:
    js_file.write(jsText)
