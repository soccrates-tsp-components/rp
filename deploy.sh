#!/bin/bash

tag=${DOCKER_TAG:-1.7.4}
printf "tag is %s\n" "$tag"

env TAG=${tag} docker stack deploy --prune --with-registry-auth \
    -c docker-compose.yml rp
