from enum import Enum, unique
import ipaddress
import json

from app.containmentDefense import ContainmentDefense, DefenseName
from app.siemLabel import SiemLabel

# Constants
INTERNAL_NETWORKS_LIST = [ipaddress.ip_network("10.0.0.0/8"),
                          ipaddress.ip_network("172.16.0.0/12"),
                          ipaddress.ip_network("192.168.0.0/16"),
                          ipaddress.ip_network('fd00::/8')]


@unique
class GenerationType(Enum):
    CONTAINMENT = "containment"
    STOP_EXFILTRATION = "stop_exfiltration"


def gen_containment_coas(type, sourceIPs, destinationIPs, siem_label_str):
    siem_label = SiemLabel(siem_label_str)
    generationType = GenerationType(type)
    if siem_label.isInitialAccess():
        defenses_list = generate_intial_access_defense(sourceIPs, destinationIPs)
    elif siem_label.isLateralMovement():
        defenses_list = generate_lateral_movement_defense(sourceIPs, destinationIPs)
    elif siem_label.isDataExfiltration():
        defenses_list = generate_data_exfiltration_defense(generationType, sourceIPs, destinationIPs)
    else:
        #Default Defense, contain internal IPs.
        defenses_list = generate_default_defense(generationType, sourceIPs, destinationIPs)
    containment_coa = {"defenses":defenses_list}
    coas = [containment_coa]
    return {'containment':True, 'coas': coas}


def generate_intial_access_defense(sourceIPs, destinationIPs):
    """Generate defenses to Isolate internal IPs."""
    internalIPs = get_internal_IPs(sourceIPs + destinationIPs)
    defenses_list = []
    if internalIPs:
        hostIsolationDefense = generate_host_isolation_defense(internalIPs)
        defenses_list.append(hostIsolationDefense)
    # Block flows between source and destination
    flows = get_flows(sourceIPs, destinationIPs)
    traficFilteringDefense = gen_traffic_filtering_defense(flows)
    defenses_list.append(traficFilteringDefense)
    return defenses_list


def generate_lateral_movement_defense(sourceIPs, destinationIPs):
    """Generate defenses to Isolate internal IPs."""
    internalIPs = get_internal_IPs(sourceIPs + destinationIPs)
    defenses_list = []
    if internalIPs:
        hostIsolationDefense = generate_host_isolation_defense(internalIPs)
        defenses_list.append(hostIsolationDefense)
    return defenses_list


def generate_data_exfiltration_defense(generationType, sourceIPs, destinationIPs):
    """Generate defenses to Isolate internal IPs and filter trafic. """
    defenses_list = []
    if generationType == GenerationType.CONTAINMENT:
        internalIPs = get_internal_IPs(sourceIPs + destinationIPs)
        if internalIPs:
            hostIsolationDefense = generate_host_isolation_defense(internalIPs)
            defenses_list.append(hostIsolationDefense)
    elif generationType == GenerationType.STOP_EXFILTRATION:
        flows = get_flows(sourceIPs, destinationIPs)
        traficFilteringDefense = gen_traffic_filtering_defense(flows)
        defenses_list.append(traficFilteringDefense)
    return defenses_list


def generate_default_defense(generationType, sourceIPs, destinationIPs):
    """Generate default defenses list to contain internal IPs of filter traffic,
    depending on the generationType."""
    defenses_list = generate_data_exfiltration_defense(generationType, sourceIPs, destinationIPs)
    return defenses_list


def generate_host_isolation_defense(hosts_ip):
    containmentDef = ContainmentDefense(DefenseName.HOST_ISOLATION, hosts_ip)
    return containmentDef.asdict()


def gen_traffic_filtering_defense(flows):
    containmentDef = ContainmentDefense(DefenseName.TRAFFIC_FILTERING, flows)
    return containmentDef.asdict()


def get_flows(sourceIPs, destinationIPs):
    flows = [{"src":sIP, "dst":dIP} for sIP in set(sourceIPs) for dIP in set(destinationIPs)]
    return flows


def get_internal_IPs(address_list):
    internalIPs = set([ip for ip in address_list if is_internal_IP(ip)])
    return internalIPs


def is_internal_IP(ip):
    ip_address = ipaddress.ip_address(ip)
    for network in INTERNAL_NETWORKS_LIST:
        if ip_address in network:
            return True
    return False
