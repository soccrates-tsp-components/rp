import json
import time

import app.rp_config as rp_config


class CoaSaver():
    def __init__(self, coas, containment, ale='', aiv=''):
        self.coas = coas
        self.containment = containment
        self.rp_gui_address = rp_config.RP_GUI_ADDRESS
        self.aiv = aiv
        self.ale = ale
        self.set_coa_ids()


    def save_to_json(self):
        self.data_to_save = {"coas": self.coas,
                            "containment": self.containment,
                            "ale": self.ale,
                            "aiv": self.aiv}
        self.set_request_uid()
        self.set_file_name()
        with open(self.fileName, 'w') as f:
            json.dump(self.data_to_save, f)
        self.set_report_url()


    def set_coa_ids(self):
        for i, coa in enumerate(self.coas):
            coa["id"] = i
            coa["name"] = f'COA {i+1}'


    def set_request_uid(self):
        self.request_uid = str(time.time_ns())


    def set_file_name(self):
        self.fileName = f'{rp_config.PREFIX_FILENAME}{self.request_uid}{rp_config.SUFFIX_FILENAME}'

    def get_report_url(self):
        return self.report_url

    def set_report_url(self):
        self.report_url = f'{self.rp_gui_address}/?request={self.request_uid}'
